ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
IMAGE_DEPS=$(shell cat network.dot | sed 's/src="/\nsrc="/' | grep '^src="' | sed -E 's/src="([^"]*)".*/\1/')

.PHONY: all
all: network.png

.PHONY: clean
clean:
	rm network.png
	rm network.svg
	rm icons/*.png

%.png: %.svg
	inkscape "$<" -C --export-type=png --export-dpi=1024 -o "$@"

network.png: network.svg
	inkscape "$<" -D --export-type=png --export-dpi=192 -o "$@"

network.svg: network.dot $(IMAGE_DEPS)
	dot -Tsvg "$<" "-o$@"
